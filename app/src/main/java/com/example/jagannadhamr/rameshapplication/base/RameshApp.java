package com.example.jagannadhamr.rameshapplication.base;

import com.example.jagannadhamr.rameshapplication.R;

public class RameshApp extends RameshAppBase {
    @Override
    protected RameshAppComponent createApplicationComponent() {
        String baseApiUrl = getResources().getString(R.string.base_api_url);
        return DaggerRameshAppComponent.builder()
                .rameshAppModule(new RameshAppModule(this))
//                .networkModule(new NetworkModule(baseApiUrl))
                .build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
