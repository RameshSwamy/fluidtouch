package com.example.jagannadhamr.rameshapplication.base;

import android.support.multidex.MultiDexApplication;
import android.util.Log;

public abstract class RameshAppBase extends MultiDexApplication {
    private static RameshAppBase sInstance;
    private static RameshAppComponent mApplicationComponent;

    protected abstract RameshAppComponent createApplicationComponent();

    // Provide easy access to the main application instance
    public static RameshAppBase get() {
        return sInstance;
    }

    public static RameshAppComponent getComponent() {
        return mApplicationComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        configureDependencyInjection();
    }

    protected String logTag() {
        return getClass().getSimpleName();
    }

    private void configureDependencyInjection() {
        Log.v(logTag(), "Configuring dependency injection...");
        mApplicationComponent = createApplicationComponent();
    }




}
