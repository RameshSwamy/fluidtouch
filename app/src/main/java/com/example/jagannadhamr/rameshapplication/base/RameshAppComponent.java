package com.example.jagannadhamr.rameshapplication.base;

import com.example.jagannadhamr.rameshapplication.feature.launcher.LauncherActivityComponent;
import com.example.jagannadhamr.rameshapplication.feature.launcher.LauncherActivityModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        RameshAppModule.class
})
public interface RameshAppComponent {
    LauncherActivityComponent plus(LauncherActivityModule module);

}
