package com.example.jagannadhamr.rameshapplication.base;


import android.app.Application;

import com.example.jagannadhamr.rameshapplication.utils.DefaultRxSchedulersApplier;
import com.example.jagannadhamr.rameshapplication.utils.RxBus;
import com.example.jagannadhamr.rameshapplication.utils.RxSchedulersApplier;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RameshAppModule {

    private final Application mApplication;

    public RameshAppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    RxBus provideRxBus() {
        return new RxBus();
    }


    @Provides
    RxSchedulersApplier provideRxSchedulersProvider() {
        return new DefaultRxSchedulersApplier();
    }

}
