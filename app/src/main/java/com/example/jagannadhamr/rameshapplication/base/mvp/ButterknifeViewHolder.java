package com.example.jagannadhamr.rameshapplication.base.mvp;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;


public abstract class ButterknifeViewHolder extends RecyclerView.ViewHolder {

    public ButterknifeViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

}
