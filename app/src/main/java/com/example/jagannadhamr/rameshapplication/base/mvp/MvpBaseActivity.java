package com.example.jagannadhamr.rameshapplication.base.mvp;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.jagannadhamr.rameshapplication.base.RameshAppBase;
import com.example.jagannadhamr.rameshapplication.base.RameshAppComponent;
import com.example.jagannadhamr.rameshapplication.utils.RxBus;

import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicInteger;

import javax.inject.Inject;

import butterknife.ButterKnife;


public abstract class MvpBaseActivity<P extends MvpPresenter, C> extends AppCompatActivity implements MvpView<P> {

    private boolean isRunning;
    private static AtomicInteger sAtomicInteger = new AtomicInteger();

    private String mViewIdentity = String.format("activity-%d", sAtomicInteger.incrementAndGet());

    private C mActivityComponent;
    private P mPresenter;

    protected abstract int getActivityViewId();

    protected abstract C setupActivityComponent();

    @SuppressWarnings("unchecked")
    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        Log.v(logTag(), "onCreate()");

        // This is a hook for handling things like changing the activity theme
        onCreateBeforeSuper(savedInstanceState);

        super.onCreate(savedInstanceState);

        // Set activity content view
        int viewId = getActivityViewId();
        if (viewId != 0) {
            // Initialize the injector
            Log.v(logTag(), "Setting up activity component");
            mActivityComponent = setupActivityComponent();

            // Inject the activity
            if (mActivityComponent != null) {
                Log.v(logTag(), "Injecting dependencies");
                injectActivity();

                if (mPresenter != null) {
                    Log.v(logTag(), "Trigger presenter onCreate()");
                    mPresenter.onCreate(savedInstanceState);
                }
            }

            Log.v(logTag(), "Triggering onCreateBeforeSetContentView()");
            onCreateBeforeSetContentView(savedInstanceState);

            Log.v(logTag(), "Setting activity content view");
            setContentView(viewId);

            Log.v(logTag(), "Binding activity content view");
            ButterKnife.bind(this);

            Log.v(logTag(), "Triggering onCreateAfterSetContentView()");
            onCreateAfterSetContentView(savedInstanceState);
        } else {
            Log.e(logTag(), "Invalid content view for activity");
            throw new RuntimeException("Invalid content view for activity");
        }
    }

    protected void injectActivity() {
        Object injector = getActivityComponent();
        try {
            Method injectMethod = injector.getClass().getMethod("inject", getClass());
            injectMethod.invoke(injector, this);
        } catch (Exception e) {
            Log.e(logTag(), "Error injecting fragment dependencies", e);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onResume() {
        Log.v(logTag(), "onResume()");
        super.onResume();
        isRunning = true;
        if (mPresenter != null) {
            Log.v(logTag(), "Trigger presenter subscribeToEventBus()");
            mPresenter.subscribeToEventBus();

            Log.v(logTag(), "Trigger presenter onTakeView()");
            mPresenter.onTakeView(this);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onPause() {
        Log.v(logTag(), "onPause()");
        super.onPause();
        isRunning = false;

        if (mPresenter != null) {
            Log.v(logTag(), "Trigger presenter onDestroyView()");
            mPresenter.onDestroyView(this);

            Log.v(logTag(), "Triggering presenter unsubscribeFromEventBus()");
            mPresenter.unsubscribeFromEventBus();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.v(logTag(), "onStart()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.v(logTag(), "onStop()");
    }

    protected void onCreateBeforeSuper(Bundle savedInstanceState) {
        // Do nothing by default
    }

    protected void onCreateBeforeSetContentView(Bundle savedInstanceState) {
        // Do nothing by default
    }

    protected void onCreateAfterSetContentView(Bundle savedInstanceState) {
        // Do nothing by default
    }

    protected final RameshAppComponent getApplicationComponent() {
        return ((RameshAppBase) getApplication()).getComponent();
    }

    protected final C getActivityComponent() {
        return mActivityComponent;
    }

    @Override
    public final Activity getActivityFromView() {
        return this;
    }

    @Override
    public final P getPresenter() {
        return mPresenter;
    }

    @Override
    public RxBus getEventBus() {
        if (mPresenter == null) {
            throw new RuntimeException("The presenter was not available yet");
        }
        return mPresenter.getEventBus();
    }

    @Inject
    @SuppressWarnings("unchecked")
    @Override
    public final void setupPresenter(P presenter, RxBus eventBus) {
        mPresenter = presenter;

        if (mPresenter != null) {
            mPresenter.setEventBus(eventBus);
            mPresenter.setView(this);
        }
    }

    @Override
    public String getViewIdentity() {
        return mViewIdentity;
    }

    protected String logTag() {
        return String.format("%s [%s]", getClass().getSimpleName(), mViewIdentity);
    }




}
