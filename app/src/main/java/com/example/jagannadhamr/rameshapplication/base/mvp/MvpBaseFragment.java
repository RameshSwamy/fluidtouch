package com.example.jagannadhamr.rameshapplication.base.mvp;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jagannadhamr.rameshapplication.utils.RxBus;

import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicInteger;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;


public abstract class MvpBaseFragment<P extends MvpPresenter> extends Fragment implements MvpView<P> {

    private static AtomicInteger sAtomicInteger = new AtomicInteger();

    private String mViewIdentity = String.format("fragment-%d", sAtomicInteger.incrementAndGet());

    private P mPresenter;
    private Unbinder mButterknifeUnbinder;

    protected abstract int getFragmentViewId();

    @SuppressWarnings("unchecked")
    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.v(logTag(), "onCreateView()");
        try {

            int viewId = getFragmentViewId();
            if (viewId != 0) {
                // Inject fragment
                Log.v(logTag(), "Injecting dependencies");
                injectFragment();

                if (mPresenter != null) {
                    Log.v(logTag(), String.format("%s", getPresenter().toString()));
                    Log.v(logTag(), "Trigger presenter onCreate()");
                    mPresenter.onCreate(savedInstanceState);
                }

                Log.v(logTag(), "Inflating fragment layout");
                View view = inflater.inflate(viewId, container, false);

                Log.v(logTag(), "Binding fragment view");
                mButterknifeUnbinder = ButterKnife.bind(this, view);

                Log.v(logTag(), "Calling onFragmentViewCreated()");
                onFragmentViewCreated(view, savedInstanceState);

                if (mPresenter != null) {
                    Log.v(logTag(), "Trigger presenter onTakeView()");
                    mPresenter.onTakeView(this);
                }

                return view;
            } else {
                Log.e(logTag(), "Invalid view for fragment");
                throw new RuntimeException("Invalid content view for activity");
            }
        } catch (Exception e) {
            throw new RuntimeException("Invalid content view for activity");
        }
    }

    @Override
    public final void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Do not allow overriding this methods. Use onFragmentViewCreated() instead
    }

    protected void onFragmentViewCreated(View view, Bundle savedInstanceState) {
        // Do nothing by default
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onDestroyView() {
        Log.v(logTag(), "onDestroyView()");

        // TODO: Verify this works as it's new to recente versions of Butterknife
        Log.v(logTag(), "Unbinding fragment view");
        if (mButterknifeUnbinder != null) {
            mButterknifeUnbinder.unbind();
        }

        if (mPresenter != null) {
            Log.v(logTag(), "Trigger presenter onDestroyView()");
            mPresenter.onDestroyView(this);

            Log.v(logTag(), "Trigger presenter unsubscribeFromEventBus()");
            mPresenter.unsubscribeFromEventBus();
        }


        Log.v(logTag(), "super.onDestroyView()");
        super.onDestroyView();
    }

    @SuppressWarnings("unchecked")
    @Inject
    @Override
    public void setupPresenter(P presenter, RxBus eventBus) {
        mPresenter = presenter;

        if (mPresenter != null) {
            mPresenter.setEventBus(eventBus);
            mPresenter.setView(this);

            Log.v(logTag(), "Trigger presenter subscribeToEventBus()");
            mPresenter.subscribeToEventBus();
        }
    }

    protected void injectFragment() {
        Object injector = ((MvpBaseActivity) getActivity()).getActivityComponent();
        try {
            Method injectMethod = injector.getClass().getMethod("inject", getClass());
            injectMethod.invoke(injector, this);
        } catch (Exception e) {
            Log.e(logTag(), "Error injecting fragment dependencies", e);
        }
    }


    @Override
    public Activity getActivityFromView() {
        return getActivity();
    }

    public AppCompatActivity getAppCompatActivity() {
        return (AppCompatActivity) getActivity();
    }

    @Override
    public P getPresenter() {
        return mPresenter;
    }

    @Override
    public RxBus getEventBus() {
        if (mPresenter == null) {
            throw new RuntimeException("The presenter was not available yet");
        }
        return mPresenter.getEventBus();
    }

    @Override
    public String getViewIdentity() {
        return mViewIdentity;
    }


    public String logTag() {
        return String.format("%s [%s]", getClass().getSimpleName(), mViewIdentity);
    }

    public String toString() {
        return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode());
    }


}
