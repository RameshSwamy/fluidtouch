package com.example.jagannadhamr.rameshapplication.base.mvp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.jagannadhamr.rameshapplication.utils.DefaultRxSchedulersApplier;
import com.example.jagannadhamr.rameshapplication.utils.RxBus;

import java.lang.ref.WeakReference;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;


public abstract class MvpBasePresenter<V extends MvpView> implements MvpPresenter<V> {

    private CompositeSubscription mSubscription;
    private RxBus mEventBus;
    private Subscription mEventSubscription;
    private WeakReference<V> mView;

    @Override
    public V getView() {
        return (mView != null) ? mView.get() : null;
    }

    @Override
    public void setView(V view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public boolean hasView() {
        return (mView != null);
    }

    @Override
    public RxBus getEventBus() {
        return mEventBus;
    }

    @Override
    public void setEventBus(RxBus eventBus) {
        mEventBus = eventBus;
    }

    @Override
    public Context getApplicationContext() {
        return getActivity().getApplicationContext();
    }

    @Override
    public Activity getActivity() {
        return getView().getActivityFromView();
    }

    @Override
    public SharedPreferences getSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
    }

    @Override
    public void onTakeView(V view) {
    }

    @Override
    public void reloadData() {
    }

    @Override
    public void onDestroyView(V view) {
        if (mSubscription != null && mSubscription.hasSubscriptions()
                && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        mSubscription = null;
    }

    protected void onEventReceived(Object event) {
        // Do nothing by default
    }

    @Override
    public void subscribeToEventBus() {
        // Subscribe to the event bus to receive UI events
        Log.v(logTag(), "Subscribing presenter to the event bus");
        if (mEventSubscription == null || mEventSubscription.isUnsubscribed()) {
            mEventSubscription = getEventBus().observable()
                    .compose(DefaultRxSchedulersApplier.applySchedulers())
                    .subscribe(event -> {
                        if (getActivity().isFinishing() || getActivity().isDestroyed()) {
                            Log.d(logTag(), "Skipping event delivery as the activity is finishing or destroyed");
                        } else {
                            Log.d(logTag(), String.format("Received event [type=%s]", event.getClass().getSimpleName()));
                            onEventReceived(event);
                        }
                    });
        }
    }

    @Override
    public void unsubscribeFromEventBus() {
        // Unsubscribe from the event bus to receive UI events
        Log.v(logTag(), "Unsubscribing presenter from the event bus");
        if (mEventSubscription != null && !mEventSubscription.isUnsubscribed()) {
            mEventSubscription.unsubscribe();
            mEventSubscription = null;
        }
    }

    protected void addSubscription(Subscription subscription) {
        if (mSubscription == null) {
            mSubscription = new CompositeSubscription();
        }
        mSubscription.add(subscription);
    }

    protected void removeSubscription(Subscription subscription) {
        if (subscription != null) {
            if (!subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
            mSubscription.remove(subscription);
        }
    }

    protected String logTag() {
        String logTag = "";
        try {
            logTag = String.format("%s [%s]", getClass().getSimpleName(), getView().getViewIdentity());
        } catch (Exception e) {
            logTag = "KantarViewIdentityFailed";
        }
        return logTag;
    }


}
