package com.example.jagannadhamr.rameshapplication.base.mvp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.example.jagannadhamr.rameshapplication.utils.RxBus;


public interface MvpPresenter<V> {

    Activity getActivity();

    Context getApplicationContext();

    SharedPreferences getSharedPreferences();

    void onCreate(Bundle savedInstanceState);

    void onTakeView(V view);

    void reloadData();

    void onDestroyView(V view);

    V getView();

    void setView(V view);

    boolean hasView();

    RxBus getEventBus();

    void setEventBus(RxBus eventBus);

    void subscribeToEventBus();

    void unsubscribeFromEventBus();
}
