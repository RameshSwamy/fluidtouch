package com.example.jagannadhamr.rameshapplication.base.mvp;

import android.app.Activity;
import android.support.annotation.StringRes;
import android.util.DisplayMetrics;

import com.example.jagannadhamr.rameshapplication.utils.RxBus;


public interface MvpView<P> {

    Activity getActivityFromView();

    String getViewIdentity();

    void setupPresenter(P presenter, RxBus eventBus);

    P getPresenter();

    RxBus getEventBus();
}
