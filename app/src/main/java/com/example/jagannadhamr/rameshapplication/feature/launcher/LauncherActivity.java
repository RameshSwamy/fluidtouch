package com.example.jagannadhamr.rameshapplication.feature.launcher;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.jagannadhamr.rameshapplication.R;
import com.example.jagannadhamr.rameshapplication.base.mvp.MvpBaseActivity;
import com.example.jagannadhamr.rameshapplication.feature.launcher.splash.SplashFragment;
import com.example.jagannadhamr.rameshapplication.feature.launcher.welcome.WelcomeFragment;


public class LauncherActivity extends MvpBaseActivity<LauncherActivityPresenter, LauncherActivityComponent>
        implements LauncherActivityView {

    @Override
    protected int getActivityViewId() {
        return R.layout.activity_launcher;
    }

    @Override
    protected LauncherActivityComponent setupActivityComponent() {
        return getApplicationComponent().plus(new LauncherActivityModule(this));
    }

    @Override
    protected void onCreateAfterSetContentView(Bundle savedInstanceState) {
        super.onCreateAfterSetContentView(savedInstanceState);
        getPresenter().handleLaunchIntent(getIntent());

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getPresenter().handleLaunchIntent(intent);
    }

    @Override
    public void showSplashFragment() {
        clearBackStackIfNeeded();
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.launcher_frame_layout_root, SplashFragment.newInstance())
                .commit();
    }

    @Override
    public void showWelcomeFragment() {
        clearBackStackIfNeeded();
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.launcher_frame_layout_root, WelcomeFragment.newInstance())
                .commit();
    }

    private void clearBackStackIfNeeded() {
        for (int i = 0; i < getFragmentManager().getBackStackEntryCount(); i++) {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.e("on result", "onnnnn" + requestCode);

        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.launcher_frame_layout_root);
        currentFragment.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }


}
