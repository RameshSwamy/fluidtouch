package com.example.jagannadhamr.rameshapplication.feature.launcher;

import com.example.jagannadhamr.rameshapplication.feature.launcher.splash.SplashFragment;
import com.example.jagannadhamr.rameshapplication.feature.launcher.splash.SplashFragmentModule;
import com.example.jagannadhamr.rameshapplication.feature.launcher.welcome.WelcomeFragment;
import com.example.jagannadhamr.rameshapplication.feature.launcher.welcome.WelcomeFragmentModule;
import com.example.jagannadhamr.rameshapplication.scope.ActivityScope;

import dagger.Subcomponent;


@ActivityScope
@Subcomponent(modules = {
        LauncherActivityModule.class,
        SplashFragmentModule.class,
        WelcomeFragmentModule.class
})
public interface LauncherActivityComponent {
    void inject(LauncherActivity activity);

    void inject(SplashFragment fragment);

    void inject(WelcomeFragment fragment);

}
