package com.example.jagannadhamr.rameshapplication.feature.launcher;


import android.app.Activity;

import com.example.jagannadhamr.rameshapplication.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;


@Module
public class LauncherActivityModule {

    private final Activity mActivity;

    public LauncherActivityModule(Activity activity) {
        mActivity = activity;
    }

    @Provides
    @ActivityScope
    LauncherActivityPresenter providesLauncherActivityPresenter(
           ) {
        return new LauncherActivityPresenterImpl();
    }

}
