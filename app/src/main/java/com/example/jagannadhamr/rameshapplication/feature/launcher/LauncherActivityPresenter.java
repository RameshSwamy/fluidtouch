package com.example.jagannadhamr.rameshapplication.feature.launcher;

import android.content.Intent;

import com.example.jagannadhamr.rameshapplication.base.mvp.MvpPresenter;


public interface LauncherActivityPresenter extends MvpPresenter<LauncherActivityView> {

    void handleLaunchIntent(Intent activityIntent);

}
