package com.example.jagannadhamr.rameshapplication.feature.launcher;

import android.content.Intent;
import android.util.Log;

import com.example.jagannadhamr.rameshapplication.base.mvp.MvpBasePresenter;
import com.example.jagannadhamr.rameshapplication.feature.launcher.splash.event.EventSplashTimeout;


public class LauncherActivityPresenterImpl extends MvpBasePresenter<LauncherActivityView>
        implements LauncherActivityPresenter {

    LauncherActivityPresenterImpl() {
    }

    public void handleLaunchIntent(Intent activityIntent) {
        if (Intent.ACTION_MAIN.equals(activityIntent.getAction())) {
            getView().showSplashFragment();
        }
    }

    @Override
    protected void onEventReceived(Object event) {
        Log.e("onEventReceived", "LauncherActivityPresenterImpl");
        if (event instanceof EventSplashTimeout) {
            getView().showWelcomeFragment();

        }
    }


}
