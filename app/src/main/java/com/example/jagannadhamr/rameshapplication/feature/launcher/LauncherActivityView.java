package com.example.jagannadhamr.rameshapplication.feature.launcher;


import com.example.jagannadhamr.rameshapplication.base.mvp.MvpView;

interface LauncherActivityView extends MvpView<LauncherActivityPresenter> {

    void showSplashFragment();

    void showWelcomeFragment();

}
