package com.example.jagannadhamr.rameshapplication.feature.launcher.splash;

import android.os.Bundle;

import com.example.jagannadhamr.rameshapplication.R;
import com.example.jagannadhamr.rameshapplication.base.mvp.MvpBaseFragment;


public class SplashFragment extends MvpBaseFragment<SplashFragmentPresenter>
        implements SplashFragmentView {

    public static SplashFragment newInstance() {
        Bundle args = new Bundle();

        SplashFragment fragment = new SplashFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentViewId() {
        return R.layout.fragment_splash;
    }

}
