package com.example.jagannadhamr.rameshapplication.feature.launcher.splash;


import com.example.jagannadhamr.rameshapplication.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;


@Module
public class SplashFragmentModule {

    @Provides
    @ActivityScope
    SplashFragmentPresenter providesSplashFragmentPresenter() {
        return new SplashFragmentPresenterImpl();
    }


}
