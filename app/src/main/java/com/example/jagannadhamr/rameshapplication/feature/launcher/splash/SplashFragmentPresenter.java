package com.example.jagannadhamr.rameshapplication.feature.launcher.splash;


import com.example.jagannadhamr.rameshapplication.base.mvp.MvpPresenter;

public interface SplashFragmentPresenter extends MvpPresenter<SplashFragmentView> {

}
