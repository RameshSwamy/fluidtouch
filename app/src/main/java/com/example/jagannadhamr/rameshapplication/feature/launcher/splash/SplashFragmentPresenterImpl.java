package com.example.jagannadhamr.rameshapplication.feature.launcher.splash;


import android.util.Log;

import com.example.jagannadhamr.rameshapplication.base.mvp.MvpBasePresenter;
import com.example.jagannadhamr.rameshapplication.feature.launcher.splash.event.EventSplashTimeout;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class SplashFragmentPresenterImpl extends MvpBasePresenter<SplashFragmentView>
        implements SplashFragmentPresenter {

    private static final long SPLASH_DELAY = 2000;

    public SplashFragmentPresenterImpl() {

    }

    @Override
    public void onTakeView(SplashFragmentView view) {
        super.onTakeView(view);
        Log.e("onTakeView", "SplashFragmentPresenterImpl");

        // Start the main activity after a little delay
        addSubscription(Observable.timer(SPLASH_DELAY, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> {
                    getEventBus().post(new EventSplashTimeout());
                })
        );

    }

}
