package com.example.jagannadhamr.rameshapplication.feature.launcher.splash;


import com.example.jagannadhamr.rameshapplication.base.mvp.MvpView;

public interface SplashFragmentView extends MvpView<SplashFragmentPresenter> {

}
