package com.example.jagannadhamr.rameshapplication.feature.launcher.welcome;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.jagannadhamr.rameshapplication.R;
import com.example.jagannadhamr.rameshapplication.base.mvp.MvpBaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


public class WelcomeFragment extends MvpBaseFragment<WelcomeFragmentPresenter>
        implements WelcomeFragmentView {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    int size = 0;


    public static WelcomeFragment newInstance() {
        Bundle args = new Bundle();

        WelcomeFragment fragment = new WelcomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentViewId() {
        return R.layout.fragment_welcome;
    }

    @Override
    protected void onFragmentViewCreated(View view, Bundle savedInstanceState) {
        super.onFragmentViewCreated(view, savedInstanceState);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE); //Получаем размер экрана
        Display display = wm.getDefaultDisplay();

        Point point = new Point();
        display.getSize(point);
        int screenWidth = point.x; //Ширина экрана

//        int photoWidth = 384;//(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, this.getResources().getDisplayMetrics()); //Переводим в точки
        int columnsCount = 4;//screenWidth / photoWidth; //Число столбцов
        size = screenWidth / columnsCount;

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), columnsCount);
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 5);


        mRecyclerView.setLayoutManager(gridLayoutManager);

        getPresenter().loaddata();

    }

    @Override
    public void onResume() {
        super.onResume();
        getPresenter().loaddata();
    }

    @Override
    public void updateView(List<String> albumRecords) {

        MyAdapter myAdapter = new MyAdapter(albumRecords, size);
        mRecyclerView.setAdapter(myAdapter);
    }


    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
        List<String> books = new ArrayList<>();
        int size = 0;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView book;

            public MyViewHolder(View v) {
                super(v);
                book = v.findViewById(R.id.image);
            }
        }

        public MyAdapter(List<String> items, int size) {
            books = items;
            this.size = size;
        }


        @Override
        public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.book, parent, false);
            RelativeLayout relativeLayout = v.findViewById(R.id.root);
            relativeLayout.getLayoutParams().height = size;
            relativeLayout.getLayoutParams().width = size;

            MyViewHolder vh = new MyViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return books.size();
        }
    }


}
