package com.example.jagannadhamr.rameshapplication.feature.launcher.welcome;


import com.example.jagannadhamr.rameshapplication.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;


@Module
public class WelcomeFragmentModule {

    @Provides
    @ActivityScope
    WelcomeFragmentPresenter providesWelcomeFragmentPresenter() {
        return new WelcomeFragmentPresenterImpl();
    }

}
