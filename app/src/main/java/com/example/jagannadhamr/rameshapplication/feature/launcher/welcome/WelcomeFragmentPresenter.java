package com.example.jagannadhamr.rameshapplication.feature.launcher.welcome;


import com.example.jagannadhamr.rameshapplication.base.mvp.MvpPresenter;

public interface WelcomeFragmentPresenter extends MvpPresenter<WelcomeFragmentView> {
    void loaddata();

    void onPullToRefresh();

}
