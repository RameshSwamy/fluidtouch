package com.example.jagannadhamr.rameshapplication.feature.launcher.welcome;

import android.util.Log;

import com.example.jagannadhamr.rameshapplication.base.mvp.MvpBasePresenter;

import java.util.ArrayList;
import java.util.List;


public class WelcomeFragmentPresenterImpl extends MvpBasePresenter<WelcomeFragmentView>
        implements WelcomeFragmentPresenter {


    public WelcomeFragmentPresenterImpl() {
    }

    @Override
    public void onTakeView(WelcomeFragmentView view) {
        super.onTakeView(view);
        Log.e("onTakeView", "WelcomeFragmentPresenterImpl");
        updateViews();
    }

    private void updateViews() {

        List<String> items = new ArrayList<>();
        for (int i = 0; i < 16; i++)
            items.add("" + i);

        getView().updateView(items);
    }

    @Override
    public void loaddata() {
        updateViews();
    }

    @Override
    public void onPullToRefresh() {
        updateViews();
    }
}
