package com.example.jagannadhamr.rameshapplication.feature.launcher.welcome;


import com.example.jagannadhamr.rameshapplication.base.mvp.MvpView;

import java.util.List;

public interface WelcomeFragmentView extends MvpView<WelcomeFragmentPresenter> {
    void updateView(List<String> albumRecords);

}
