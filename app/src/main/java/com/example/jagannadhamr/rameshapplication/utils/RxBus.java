package com.example.jagannadhamr.rameshapplication.utils;

import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;


public class RxBus {

    private final Subject<Object, Object> mEventBus = new SerializedSubject<>(PublishSubject.create());

    public void post(Object o) {
        // Only dispatch if an observer is registered
        if (mEventBus.hasObservers()) {
            mEventBus.onNext(o);
        }
    }

    public Observable<Object> observable() {
        return mEventBus;
    }

    public boolean hasObservers() {
        return mEventBus.hasObservers();
    }

}
