package com.example.jagannadhamr.rameshapplication.utils;

import rx.Observable;

public interface RxSchedulersApplier {
    <T> Observable.Transformer<T, T> apply();
}
